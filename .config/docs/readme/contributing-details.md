## Contributing

Contributions, issues, and feature requests are welcome! Feel free to check the [issues page]({{ repository.group.dockerfile }}/{{ subgroup }}/{{ slug }}/-/issues). If you would like to contribute, please take a look at the [contributing guide]({{ repository.group.dockerfile }}/{{ subgroup }}/{{ slug }}/-/blob/master/CONTRIBUTING.md).

<details>
<summary>Sponsorship</summary>
<br/>
<blockquote>
<br/>
I create open source projects out of love. Although I have a job, shelter, and as much fast food as I can handle, it would still be pretty cool to be appreciated by the community for something I have spent a lot of time and money on. Please consider sponsoring me! Who knows? Maybe I will be able to quit my job and publish open source full time.
<br/><br/>Sincerely,<br/><br/>

**_{{ author_fullname }}_**<br/><br/>

</blockquote>

<a href="{{ profile.patreon }}">
  <img src="{{ assets.patreon_image }}" width="160">
</a>

</details>
